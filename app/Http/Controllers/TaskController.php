<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $tasks = Task::where('user_id', auth()->user()->id)->paginate(15);
    	return view('home')->with('tasks', $tasks);
    }

    public function store(Request $request)
    {
    	$tasks = Task::where('user_id', auth()->user()->id)->get();
        // returns validated data as array
        $data = $request->validate([
        	'title' => 'required|between:2,20',
        	'description' => 'between:2,500',
        	'time_spent' => 'between:1,50',
        	'user_id' => 'required|between:1,50'
        ]);

        $task = Task::create($data);

        return back()->with(['task-created' => 'Task Created!']);
    }

   	public function updateLoad($id=0)
    {
        $tasks = Task::where('id',$id)->first();
    	return view('edit-task')->with('tasks', $tasks);
    }

 	public function update(Request $request)
    {
        // returns validated data as array
        $data = $request->validate([
        	'title' => 'required|between:2,20',
        	'description' => 'between:2,500',
        	'time_spent' => 'between:1,50',
        ]);



        $task = Task::where('id', $request->id)->update($data);

        return back()->with(['task-updated' => 'Task Updated!']);
    }

    public function delete($id = 0)
    {
        if($id != 0){

	      Task::destroy($id);

	      return back()->with(['task-deleted' => 'Task Deleted!']);
	      
	    }

    }
}
