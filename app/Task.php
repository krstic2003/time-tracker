<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
	/**
     * {@inheritDoc}
     */
    protected $fillable = ['title', 'description', 'time_spent', 'user_id'];

    /**
    * {@inheritDoc}
    */
    protected $with = ['user'];


	 /**
	 * Get associated user.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get associated timers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function timers()
    {
        return $this->hasMany(Timer::class);
    }

}
