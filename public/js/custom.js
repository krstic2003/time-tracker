jQuery(document).ready(function($){

	$('#timer').click(function(){ 
        
        var startTime = $("#task-time").val();
        //console.log(startTime);
        //ms
        startTime = parseFloat(startTime) * 60000;

        $(this).find("span").stopwatch({startTime: startTime}).stopwatch('toggle');

        var timeElapsed = $(this).find("span").text();
        timeElapsed = timeElapsed.split(":");

        console.log(timeElapsed);

        timeElapsed = ( parseFloat(timeElapsed[0]) * 3600000 ) + ( parseFloat(timeElapsed[1]) * 60000 ) + ( parseFloat(timeElapsed[2]) * 1000 );

        // new time in min
        if (!$(this).hasClass("stopped")) {
        	var newTime = parseFloat(timeElapsed) / 60000;
 
        }else{
        	var newTime = parseFloat(startTime) / 60000;
        	
        }
        

        $("#task-time").val(newTime.toFixed(2)).change();

        $(this).toggleClass("stopped");

    });

	$("#add-task").on('click', function(e){
		if ( $("#task-title").val().length < 2 ) {
			e.preventDefault();
			alert("Task Title must contain at least 2 characters!");
		}

		if ( $("#task-title").val().length < 2 ) {
			e.preventDefault();
			alert("Task Description must contain at least 2 characters!");
		}

		if ( ! $.isNumeric($("#task-time").val()) ) {
			e.preventDefault();
			alert("Task Time must be numeric!");
		}
	})

})