
## Instructions

1. Upload all files (except ttrack.sql) to your server.
2. Import ttrack.sql to your database.
3. Update db connection in .env file which is located under root folder.

## Functionalites and other comments

- View list of tasks
- Create/Edit/Delete tasks
- Under Edit page, you can use Stopwatch to mesure time instead of you.
- There is basic JS field validation for title and description (must be > 2 char) and time (must be numeric).
- Pagination is set to 15 records.
- Outside of Laravel Scripts: [jQuery Stopwatch - https://github.com/robcowie/jquery-stopwatch](https://github.com/robcowie/jquery-stopwatch)
