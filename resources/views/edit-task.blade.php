@extends('layouts.app')

@section('content')
<div class="container home">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (session('task-updated'))
                        <div class="alert alert-success" role="alert">
                            {{ session('task-updated') }}
                        </div>
                    @endif

                    <h2>Edit Task</h2>

                    <form method="POST" action="{{ url('/update') }}">
                        @csrf

                        <div class="form-group">
                            <label for="task-title">Title</label>
                            <input type="text" class="form-control" name="title" id="task-title" aria-describedby="title" value="{{ $tasks->title }}">
                        </div>

                        <div class="form-group">
                            <label for="task-desc">Description</label>
                            <textarea class="form-control" name="description" id="task-desc" aria-describedby="description" rows="1">{{ $tasks->description }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="task-time">Time Spent (minutes)</label>
                            <input type="text" class="form-control" name="time_spent" id="task-time" aria-describedby="time spent" value="{{ $tasks->time_spent }}" >
                        </div>
                        <div class="clearfix"></div>
                        <div id="timer" class="stopped">
                            Click me to Start/Stop
                            <span >00:00:00</span>

                        </div>
                        <input type="hidden" name="id" value="{{ $tasks->id }}">
                        
                        <button type="submit" class="btn btn-success">Update</button> <button class="btn btn-primary"><a href="{{ url('/home') }}">Back to List</a></button>

                        <small>*After stopping counter please hit the Update button in order to save changes.</small>
                    </form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection