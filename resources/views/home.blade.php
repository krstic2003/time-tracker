@extends('layouts.app')

@section('content')
<div class="container home">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (session('task-created'))
                        <div class="alert alert-success" role="alert">
                            {{ session('task-created') }}
                        </div>
                    @endif

                    @if (session('task-deleted'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('task-deleted') }}
                        </div>
                    @endif

                    <h2>Add Task</h2>

                    <form method="POST" action="{{ url('/home') }}">
                        @csrf

                        <div class="form-group">
                            <label for="task-title">Title *</label>
                            <input type="text" class="form-control" name="title" id="task-title" aria-describedby="title" placeholder="Title">
                        </div>

                        <div class="form-group">
                            <label for="task-desc">Description *</label>
                            <textarea class="form-control" name="description" id="task-desc" aria-describedby="description" placeholder="Description" rows="1"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="task-time">Time Spent (minutes)</label>
                            <input type="text" class="form-control" name="time_spent" id="task-time" aria-describedby="time spent" placeholder="Time Spent" value="0">
                        </div>
                        <div class="clearfix"></div>
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                        <button id="add-task" type="submit" class="btn btn-primary">Create</button>
                    </form>

                    <h2>Task List</h2>
                    
                    <table class="table">
                        <thead>
                            <tr>
                              <th scope="col">Title</th>
                              <th scope="col">Description</th>
                              <th scope="col">Time</th>
                              <th scope="col" colspan="3">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tasks as $row)
                            <tr>
                                <td>{{$row->title}}</td>
                                <td>{{$row->description}}</td>
                                <td>{{$row->time_spent}}</td>
                                <td>
                                    <button type="button" class="btn btn-success"><a href="{{ url('/edit-task') }}/{{$row->id}}">Edit</a></button>
                                    <button type="button" class="btn btn-danger"><a href="{{ url('/delete') }}/{{$row->id}}">Delete</a></button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $tasks->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
